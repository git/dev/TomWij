# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"

JAVA_PKG_IUSE="test"

inherit java-pkg-2 java-ant-2 java-osgi

DESCRIPTION="Eclipse Jetty Continuation"
HOMEPAGE="http://www.eclipse.org/jetty/"
LICENSE="Apache-2.0 EPL-1.0"

BUILD_DATE="20130312"

MY_PV="${PV}.v${BUILD_DATE}"
MY_P="${PN}-${MY_PV}"

SRC_URI="http://repo1.maven.org/maven2/org/eclipse/jetty/${PN}/${MY_PV}/${MY_P}-sources.jar"
SLOT="8"
KEYWORDS="~amd64 ~x86"

S="${WORKDIR}"

COMMON_DEPEND="dev-java/jetty-util:8
	java-virtuals/servlet-api:3.0"

DEPEND="${COMMON_DEPEND}
	test? (
		dev-java/ant-junit:0
		dev-java/junit:0
	)
	>=virtual/jdk-1.7"

RDEPEND="${COMMON_DEPEND}
	>=virtual/jre-1.7"

java_prepare() {
	cd "${S}" || die
	cp "${FILESDIR}"/${P}-build.xml build.xml || die
	sed -i 's/manifest=".*"/manifest="META-INF\/MANIFEST.MF"/g' build.xml || die
	sed -i 's/\(<mkdir dir="${maven.build.outputDir}"\/>\)/\1<javac srcdir="." destdir="${maven.build.outputDir}" \/>/g' build.xml || die

	epatch "${FILESDIR}"/${P}-j6c.patch

	for FILE in $(find . -name '*.java') ; do
		echo "Processing ${FILE} ..."
		sed -i 's/org.mortbay\(\|.util\)/org.eclipse.jetty.util/g' ${FILE} || die
	done
}

EANT_GENTOO_CLASSPATH="jetty-util-8,servlet-api-3.0"
EANT_TEST_GENTOO_CLASSPATH="junit"
JAVA_ANT_REWRITE_CLASSPATH="true"

src_test() {
	java-pkg-2_src_test
}

src_install() {
	java-osgi_newjar-fromfile target/${MY_P}.jar META-INF/MANIFEST.MF org.eclipse.jetty.continuation
}